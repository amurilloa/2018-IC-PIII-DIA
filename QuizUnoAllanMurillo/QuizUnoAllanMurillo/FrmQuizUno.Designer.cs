﻿namespace QuizUnoAllanMurillo
{
    partial class FrmQuizUno
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.ternasPitagóricasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ternaPitagóricaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.triangulosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.evaluarEcuaciónToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sumatoriaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.evaluarFunciónToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rtxtResultado = new System.Windows.Forms.RichTextBox();
            this.txtParametro = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ternasPitagóricasToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(856, 40);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // ternasPitagóricasToolStripMenuItem
            // 
            this.ternasPitagóricasToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ternaPitagóricaToolStripMenuItem,
            this.triangulosToolStripMenuItem,
            this.evaluarEcuaciónToolStripMenuItem,
            this.sumatoriaToolStripMenuItem,
            this.evaluarFunciónToolStripMenuItem});
            this.ternasPitagóricasToolStripMenuItem.Name = "ternasPitagóricasToolStripMenuItem";
            this.ternasPitagóricasToolStripMenuItem.Size = new System.Drawing.Size(124, 36);
            this.ternasPitagóricasToolStripMenuItem.Text = "Ejercicios";
            // 
            // ternaPitagóricaToolStripMenuItem
            // 
            this.ternaPitagóricaToolStripMenuItem.Name = "ternaPitagóricaToolStripMenuItem";
            this.ternaPitagóricaToolStripMenuItem.Size = new System.Drawing.Size(291, 38);
            this.ternaPitagóricaToolStripMenuItem.Text = "Terna Pitagórica";
            this.ternaPitagóricaToolStripMenuItem.Click += new System.EventHandler(this.TernaPitagorica);
            // 
            // triangulosToolStripMenuItem
            // 
            this.triangulosToolStripMenuItem.Name = "triangulosToolStripMenuItem";
            this.triangulosToolStripMenuItem.Size = new System.Drawing.Size(291, 38);
            this.triangulosToolStripMenuItem.Text = "Triangulos";
            // 
            // evaluarEcuaciónToolStripMenuItem
            // 
            this.evaluarEcuaciónToolStripMenuItem.Name = "evaluarEcuaciónToolStripMenuItem";
            this.evaluarEcuaciónToolStripMenuItem.Size = new System.Drawing.Size(291, 38);
            this.evaluarEcuaciónToolStripMenuItem.Text = "Evaluar Ecuación";
            this.evaluarEcuaciónToolStripMenuItem.Click += new System.EventHandler(this.EvaluarEcuacion);
            // 
            // sumatoriaToolStripMenuItem
            // 
            this.sumatoriaToolStripMenuItem.Name = "sumatoriaToolStripMenuItem";
            this.sumatoriaToolStripMenuItem.Size = new System.Drawing.Size(291, 38);
            this.sumatoriaToolStripMenuItem.Text = "Sumatoria";
            this.sumatoriaToolStripMenuItem.Click += new System.EventHandler(this.Sumatoria);
            // 
            // evaluarFunciónToolStripMenuItem
            // 
            this.evaluarFunciónToolStripMenuItem.Name = "evaluarFunciónToolStripMenuItem";
            this.evaluarFunciónToolStripMenuItem.Size = new System.Drawing.Size(291, 38);
            this.evaluarFunciónToolStripMenuItem.Text = "Evaluar Función";
            this.evaluarFunciónToolStripMenuItem.Click += new System.EventHandler(this.EvaluarFuncion);
            // 
            // rtxtResultado
            // 
            this.rtxtResultado.Location = new System.Drawing.Point(12, 86);
            this.rtxtResultado.Name = "rtxtResultado";
            this.rtxtResultado.Size = new System.Drawing.Size(832, 487);
            this.rtxtResultado.TabIndex = 2;
            this.rtxtResultado.Text = "";
            // 
            // txtParametro
            // 
            this.txtParametro.Location = new System.Drawing.Point(186, 49);
            this.txtParametro.Name = "txtParametro";
            this.txtParametro.Size = new System.Drawing.Size(100, 31);
            this.txtParametro.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 49);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(168, 25);
            this.label1.TabIndex = 4;
            this.label1.Text = "Parametro Uno: ";
            // 
            // FrmQuizUno
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(856, 595);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtParametro);
            this.Controls.Add(this.rtxtResultado);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "FrmQuizUno";
            this.Text = "Quiz Uno";
            this.Load += new System.EventHandler(this.FrmQuizUno_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem ternasPitagóricasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ternaPitagóricaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem triangulosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem evaluarEcuaciónToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sumatoriaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem evaluarFunciónToolStripMenuItem;
        private System.Windows.Forms.RichTextBox rtxtResultado;
        private System.Windows.Forms.TextBox txtParametro;
        private System.Windows.Forms.Label label1;
    }
}

