﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuizUnoAllanMurillo
{
    public partial class FrmQuizUno : Form
    {
        private Logica log;

        public FrmQuizUno()
        {
            InitializeComponent();
        }

        private void TernaPitagorica(object sender, EventArgs e)
        {
            rtxtResultado.Text = log.BuscarTernas();
        }

        private void FrmQuizUno_Load(object sender, EventArgs e)
        {
            log = new Logica();
        }

        private void EvaluarEcuacion(object sender, EventArgs e)
        {
            rtxtResultado.Text = log.EvaluarEcuacion();
        }

        private void Sumatoria(object sender, EventArgs e)
        {
            double lim = 0;
            if (Double.TryParse(txtParametro.Text.Trim(), out lim))
            {
                rtxtResultado.Text = log.Sumatoria(lim);
            }
            else
            {
                rtxtResultado.Text = "Favor especificar un límite en el parámetro 1";
            }
        }

        private void EvaluarFuncion(object sender, EventArgs e)
        {
            rtxtResultado.Text = log.EvaluarFuncion();
        }
    }
}
