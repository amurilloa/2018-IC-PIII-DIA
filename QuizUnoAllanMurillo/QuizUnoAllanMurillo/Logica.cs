﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuizUnoAllanMurillo
{
    class Logica
    {
        internal string BuscarTernas()
        {
            string ternas = "";
            //a<b<c a y b < 25
            for (int a = 0; a < 25; a++)
            {
                for (int b = a + 1; b < 25; b++)
                {
                    for (int c = b + 1; c <= 34; c++)
                    {
                        if (Math.Pow(a, 2) + Math.Pow(b, 2) == Math.Pow(c, 2))
                        {
                            ternas += String.Format("- {0},{1},{2}\n", a, b, c);
                        }
                    }
                }
            }
            return ternas;
        }

        internal string EvaluarEcuacion()
        {
            string res = "x-------------------y\n";
            double x = 20;
            while (x <= 30)
            {
                double y = Math.Pow(x, 2) - 4 * x + 6;
                res += String.Format("x: {0:F2} ----> y:{1:F2}\n", x, y);
                x += 0.5;
            }
            return res;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="limSup"></param>
        /// <returns></returns>
        internal string Sumatoria(double limSup)
        {
            string res = "";
            double sum = 0;
            double cont = 1;
            while (sum < limSup)
            {
                sum += 1 / cont;
                res += cont == 1 ? "1 " : String.Format("+ 1/{0} ", cont);
                cont++;
            }

            return res + String.Format("\nCant. Elementos: {0}", (cont - 1));
        }

        internal string EvaluarFuncion()
        {
            string res = "x-------------------y\n";
            double x = 1;
            while (x <= 2)
            {
                double y = 4* Math.Pow(x, 2) - 16 * x + 15;
                string msj = y >= 0 ? "POSITIVO" : "NO POSITIVO";
                res += String.Format("x: {0:F2} ----> y:{1:F20}\t{2}\n", x, y, msj);
                x += 0.1;
            }
            return res;
        }
    }
}
