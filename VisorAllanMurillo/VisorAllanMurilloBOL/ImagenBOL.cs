﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VisorAllanMurilloDAL;
using VisorAllanMurilloENT;

namespace VisorAllanMurilloBOL
{
    public class ImagenBOL
    {
        public EImagen Guardar(EImagen imagen)
        {
            //Validaciones
            if (String.IsNullOrEmpty(imagen.Titulo))
            {
                throw new Exception("Título requerido");
            }
            if (String.IsNullOrEmpty(imagen.Tipo))
            {
                throw new Exception("Tipo requerido");
            }
            if (String.IsNullOrEmpty(imagen.Descripcion))
            {
                throw new Exception("Descripción requerida");
            }
            if (imagen.Imagen == null)
            {
                throw new Exception("Imagen requerida");
            }
            if (imagen.Id > 0)
            {
                return new ImagenDAL().Modificar(imagen);
            }
            else
            {
                return new ImagenDAL().Insertar(imagen);
            }
        }

        public List<EImagen> CargarFotos(string filtro = "")
        {
            return new ImagenDAL().CargarTodo(filtro);
        }

        public void Eliminar(EImagen eImagen)
        {
            new ImagenDAL().Eliminar(eImagen.Id);
        }
    }
}
