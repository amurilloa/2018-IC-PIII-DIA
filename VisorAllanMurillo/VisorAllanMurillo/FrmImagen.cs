﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using VisorAllanMurilloBOL;
using VisorAllanMurilloENT;

namespace VisorAllanMurillo
{
    public partial class FrmImagen : Form
    {
        private ImagenBOL bo;
        public EImagen Imagen { get; set; }

        public FrmImagen()
        {
            InitializeComponent();
        }

        private void SubirFoto_clikc(object sender, EventArgs e)
        {
            if (fs.ShowDialog() == DialogResult.OK)
            {
                pictureBox1.Image = Image.FromFile(fs.FileName);
            }
        }

        private void Guardar_Imagen(object sender, EventArgs e)
        {
            EImagen imagen = new EImagen
            {
                Id = Imagen != null ? Imagen.Id : 0,
                Titulo = txtTitulo.Text.Trim(),
                Tipo = txtTipo.Text.Trim(),
                Descripcion = txtDesc.Text.Trim(),
                Imagen = pictureBox1.Image
            };
            imagen = bo.Guardar(imagen);
            DialogResult = DialogResult.OK;
        }

        private void FrmImagen_Load(object sender, EventArgs e)
        {
            bo = new ImagenBOL();
            CargarDatos();
        }

        private void CargarDatos()
        {
            if (Imagen != null)
            {
                txtTitulo.Text = Imagen.Titulo;
                txtTipo.Text = Imagen.Tipo;
                txtDesc.Text = Imagen.Descripcion;
                pictureBox1.Image = Imagen.Imagen;
            }
        }
    }
}
