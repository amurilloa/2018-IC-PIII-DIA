﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using VisorAllanMurillo.Properties;
using VisorAllanMurilloBOL;
using VisorAllanMurilloENT;

namespace VisorAllanMurillo
{
    public partial class Form1 : Form
    {
        private List<EImagen> fotos;
        private ImagenBOL bol;
        private int fotoSig;
        private int xP;
        private int yP;
        private int speed;

        public Form1()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {

            timer.Enabled = !timer.Enabled;
            btnPlay.BackgroundImage = timer.Enabled ? Resources.if_pause_173065 : Resources.if_play_173075;
            if (timer.Enabled)
            {
                PasarFoto();
            }
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            PasarFoto();
        }

        private void PasarFoto()
        {
            if (fotoSig < fotos.Count && fotoSig >= 0)
            {
                pictureBox1.Image = fotos.ElementAt(fotoSig).Imagen;
                fotoSig++;
            }
            else if (fotoSig < 0)
            {
                fotoSig = fotos.Count - fotoSig;
            }
            else
            {
                fotoSig = 0;
            }

        }

        private void NuevaImagenClick(object sender, EventArgs e)
        {
            FrmImagen frm = new FrmImagen();
            frm.ShowDialog();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            fotoSig = 0;
            bol = new ImagenBOL();
            fotos = bol.CargarFotos();
            xP = panel1.Location.X + Location.X;
            yP = panel1.Location.Y + Location.Y;
            cbxSpeed.SelectedIndex = 0;
            ConfigSpeed();
        }

        private void ConfigSpeed()
        {
            switch (cbxSpeed.SelectedIndex)
            {
                case 2:
                    speed = 1;
                    break;
                case 1:
                    speed = 2;
                    break;
                default:
                    speed = 3;
                    break;
            }
        }

        private void Eliminar(object sender, EventArgs e)
        {

            DialogResult result = MessageBox.Show("Está seguro que desea eliminar la foto actual",
                "Eliminar",
                MessageBoxButtons.YesNo);
            if (result == DialogResult.Yes)
            {
                bol.Eliminar(fotos.ElementAt(fotoSig - 1));
                fotos = bol.CargarFotos();
            }

        }

        private void button3_Click(object sender, EventArgs e)
        {
            timer.Stop();
            fotoSig = 0;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            PasarFoto();
            Reset();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            fotoSig -= 2;
            PasarFoto();
            Reset();
        }

        private void Reset()
        {
            if (timer.Enabled)
            {
                timer.Stop();
                timer.Start();
            }
        }

        private void EditarImagen(object sender, EventArgs e)
        {
            FrmImagen frm = new FrmImagen
            {
                Imagen = fotos.ElementAt(fotoSig - 1)
            };
            frm.ShowDialog();
            fotos = bol.CargarFotos();
        }


        private void MouseMove(object sender, MouseEventArgs e)
        {
            int x = Cursor.Position.X;
            int y = Cursor.Position.Y;


            int limX = xP + panel1.Size.Width;
            int limY = yP + panel1.Size.Height + 30;

            if (x >= xP && x <= limX && y >= yP && y <= limY)
            {
                if (panel1.Location.Y > yP - Location.Y)
                {
                    panel1.Location = new Point(panel1.Location.X, panel1.Location.Y - speed);
                }
            }
            else
            {
                if (panel1.Location.Y < yP - Location.Y + 80)
                {
                    panel1.Location = new Point(panel1.Location.X, panel1.Location.Y + speed);
                }
            }

        }


        private void Form1_LocationChanged(object sender, EventArgs e)
        {
            xP = panel1.Location.X + Location.X;
            yP = panel1.Location.Y + Location.Y;
        }

        private void IndexChange(object sender, EventArgs e)
        {
            ConfigSpeed();
        }

        private void Salir(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
