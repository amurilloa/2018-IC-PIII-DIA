﻿namespace VisorAllanMurillo
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.nuevaImagenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editarImagenActualToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.eliminarImagenActualToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.salirToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.button1 = new System.Windows.Forms.Button();
            this.btnPlay = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.timer = new System.Windows.Forms.Timer(this.components);
            this.panel1 = new System.Windows.Forms.Panel();
            this.configSpeedToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cbxSpeed = new System.Windows.Forms.ToolStripComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.contextMenuStrip1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.ContextMenuStrip = this.contextMenuStrip1;
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(1267, 779);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.MouseMove);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.nuevaImagenToolStripMenuItem,
            this.editarImagenActualToolStripMenuItem,
            this.eliminarImagenActualToolStripMenuItem,
            this.configSpeedToolStripMenuItem,
            this.salirToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(336, 228);
            this.contextMenuStrip1.Text = "Menu Principal ";
            // 
            // nuevaImagenToolStripMenuItem
            // 
            this.nuevaImagenToolStripMenuItem.Name = "nuevaImagenToolStripMenuItem";
            this.nuevaImagenToolStripMenuItem.Size = new System.Drawing.Size(335, 36);
            this.nuevaImagenToolStripMenuItem.Text = "Nueva Imagen";
            this.nuevaImagenToolStripMenuItem.Click += new System.EventHandler(this.NuevaImagenClick);
            // 
            // editarImagenActualToolStripMenuItem
            // 
            this.editarImagenActualToolStripMenuItem.Name = "editarImagenActualToolStripMenuItem";
            this.editarImagenActualToolStripMenuItem.Size = new System.Drawing.Size(335, 36);
            this.editarImagenActualToolStripMenuItem.Text = "Editar Imagen Actual";
            this.editarImagenActualToolStripMenuItem.Click += new System.EventHandler(this.EditarImagen);
            // 
            // eliminarImagenActualToolStripMenuItem
            // 
            this.eliminarImagenActualToolStripMenuItem.Name = "eliminarImagenActualToolStripMenuItem";
            this.eliminarImagenActualToolStripMenuItem.Size = new System.Drawing.Size(335, 36);
            this.eliminarImagenActualToolStripMenuItem.Text = "Eliminar Imagen Actual";
            this.eliminarImagenActualToolStripMenuItem.Click += new System.EventHandler(this.Eliminar);
            // 
            // salirToolStripMenuItem
            // 
            this.salirToolStripMenuItem.Name = "salirToolStripMenuItem";
            this.salirToolStripMenuItem.Size = new System.Drawing.Size(335, 36);
            this.salirToolStripMenuItem.Text = "Salir";
            this.salirToolStripMenuItem.Click += new System.EventHandler(this.Salir);
            // 
            // button1
            // 
            this.button1.BackgroundImage = global::VisorAllanMurillo.Properties.Resources.if_chevron_left_173178;
            this.button1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button1.Location = new System.Drawing.Point(11, 12);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(80, 80);
            this.button1.TabIndex = 1;
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            this.button1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.MouseMove);
            // 
            // btnPlay
            // 
            this.btnPlay.BackgroundImage = global::VisorAllanMurillo.Properties.Resources.if_play_173075;
            this.btnPlay.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPlay.Location = new System.Drawing.Point(97, 12);
            this.btnPlay.Name = "btnPlay";
            this.btnPlay.Size = new System.Drawing.Size(80, 80);
            this.btnPlay.TabIndex = 2;
            this.btnPlay.UseVisualStyleBackColor = true;
            this.btnPlay.Click += new System.EventHandler(this.button2_Click);
            this.btnPlay.MouseMove += new System.Windows.Forms.MouseEventHandler(this.MouseMove);
            // 
            // button3
            // 
            this.button3.BackgroundImage = global::VisorAllanMurillo.Properties.Resources.if_stop_173105;
            this.button3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button3.Location = new System.Drawing.Point(183, 12);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(80, 80);
            this.button3.TabIndex = 3;
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            this.button3.MouseMove += new System.Windows.Forms.MouseEventHandler(this.MouseMove);
            // 
            // button4
            // 
            this.button4.BackgroundImage = global::VisorAllanMurillo.Properties.Resources.if_chevron_right_173179;
            this.button4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button4.Location = new System.Drawing.Point(269, 12);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(80, 80);
            this.button4.TabIndex = 4;
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            this.button4.MouseMove += new System.Windows.Forms.MouseEventHandler(this.MouseMove);
            // 
            // timer
            // 
            this.timer.Interval = 3000;
            this.timer.Tick += new System.EventHandler(this.timer_Tick);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.button4);
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.btnPlay);
            this.panel1.Controls.Add(this.button3);
            this.panel1.Location = new System.Drawing.Point(470, 631);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(359, 105);
            this.panel1.TabIndex = 5;
            this.panel1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.MouseMove);
            // 
            // configSpeedToolStripMenuItem
            // 
            this.configSpeedToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cbxSpeed});
            this.configSpeedToolStripMenuItem.Name = "configSpeedToolStripMenuItem";
            this.configSpeedToolStripMenuItem.Size = new System.Drawing.Size(335, 36);
            this.configSpeedToolStripMenuItem.Text = "Config Speed";
            // 
            // cbxSpeed
            // 
            this.cbxSpeed.Items.AddRange(new object[] {
            "1 seg",
            "2 seg",
            "3 seg"});
            this.cbxSpeed.Name = "cbxSpeed";
            this.cbxSpeed.Size = new System.Drawing.Size(121, 40);
            this.cbxSpeed.Text = "1 seg";
            this.cbxSpeed.SelectedIndexChanged += new System.EventHandler(this.IndexChange);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1267, 779);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.pictureBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.LocationChanged += new System.EventHandler(this.Form1_LocationChanged);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.contextMenuStrip1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button btnPlay;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Timer timer;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem nuevaImagenToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editarImagenActualToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem eliminarImagenActualToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem salirToolStripMenuItem;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ToolStripMenuItem configSpeedToolStripMenuItem;
        private System.Windows.Forms.ToolStripComboBox cbxSpeed;
    }
}

