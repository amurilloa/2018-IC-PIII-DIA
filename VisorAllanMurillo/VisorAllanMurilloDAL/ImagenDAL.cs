﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VisorAllanMurilloENT;

namespace VisorAllanMurilloDAL
{
    public class ImagenDAL
    {
        public List<EImagen> CargarTodo(string filtro)
        {
            List<EImagen> imagenes = new List<EImagen>();
            using (NpgsqlConnection con = new NpgsqlConnection(Configuracion.ConStr))
            {
                //Abrir una conexion
                con.Open();
                //Definir la consulta
                string sql = @"select id, titulo, tipo, descripcion, imagen from imagen";// where
                                                                                         //lower(tipo) like @filtro";
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                //cmd.Parameters.AddWithValue("@filtro", filtro.ToLower() + "%");
                NpgsqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    imagenes.Add(CargarImagen(reader));
                }

            }
            Console.WriteLine(imagenes.Count);
            return imagenes;
        }

        public EImagen Insertar(EImagen imagen)
        {
            using (NpgsqlConnection con = new NpgsqlConnection(Configuracion.ConStr))
            {
                con.Open();
                string sql = @"INSERT INTO imagen(titulo, tipo, descripcion, imagen)
	                            VALUES (@tit, @tip, @des, @ima) returning id";
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);

                MemoryStream stream = new MemoryStream();
                imagen.Imagen.Save(stream, ImageFormat.Jpeg);
                byte[] pic = stream.ToArray();
                cmd.Parameters.AddWithValue("@ima", pic);

                cmd.Parameters.AddWithValue("@tit", imagen.Titulo);
                cmd.Parameters.AddWithValue("@tip", imagen.Tipo);
                cmd.Parameters.AddWithValue("@des", imagen.Descripcion);

                imagen.Id = Convert.ToInt32(cmd.ExecuteScalar());
                return imagen;
            }
        }

        public EImagen CargarPorId(int id)
        {
            using (NpgsqlConnection con = new NpgsqlConnection(Configuracion.ConStr))
            {
                //Abrir una conexion
                con.Open();
                //Definir la consulta
                string sql = @"select id, titulo, tipo, descripcion, imagen from imagen where id = @id";
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                cmd.Parameters.AddWithValue("@id", id);
                NpgsqlDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    return CargarImagen(reader);
                }
            }
            return null;
        }

        private EImagen CargarImagen(NpgsqlDataReader reader)
        {

            byte[] f = new byte[0];
            f = (byte[])reader["imagen"];
            MemoryStream stream = new MemoryStream(f);

            EImagen foto = new EImagen
            {
                Id = Convert.ToInt32(reader["id"]),
                Titulo = reader["titulo"].ToString(),
                Tipo = reader["tipo"].ToString(),
                Descripcion = reader["descripcion"].ToString(),
                Imagen = Image.FromStream(stream)

            };
            return foto;
        }

        public EImagen Modificar(EImagen imagen)
        {
            using (NpgsqlConnection con = new NpgsqlConnection(Configuracion.ConStr))
            {
                con.Open();
                string sql = @"UPDATE imagen
	                            SET titulo=@tit, tipo=@tip, descripcion=@des, imagen=@ima
	                            WHERE id=@id";
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);

                MemoryStream stream = new MemoryStream();
                imagen.Imagen.Save(stream, ImageFormat.Jpeg);
                byte[] pic = stream.ToArray();
                cmd.Parameters.AddWithValue("@ima", pic);

                cmd.Parameters.AddWithValue("@tit", imagen.Titulo);
                cmd.Parameters.AddWithValue("@tip", imagen.Tipo);
                cmd.Parameters.AddWithValue("@des", imagen.Descripcion);

                cmd.Parameters.AddWithValue("@id", imagen.Id);
                cmd.ExecuteNonQuery();
                return imagen;
            }
        }

        public void Eliminar(int id)
        {
            string sql = @"DELETE FROM imagen WHERE id = @id";

            using (NpgsqlConnection con = new NpgsqlConnection(Configuracion.ConStr))
            {
                con.Open();
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                cmd.Parameters.AddWithValue("@id", id);
                cmd.ExecuteNonQuery();
            }
        }
    }
}
