﻿using System.Configuration;

namespace VisorAllanMurilloDAL
{
    internal static class Configuracion
    {
        private static string conStr = ConfigurationManager.ConnectionStrings["conStr"].ConnectionString;

        public static string ConStr
        {
            get { return conStr; }
        }

    }
}