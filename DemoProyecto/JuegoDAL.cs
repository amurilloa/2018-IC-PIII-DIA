﻿using Npgsql;
using System;

namespace DemoProyecto
{
    internal class JuegoDAL
    {
        internal EPartida Join(EUsuario usuario)
        {

            using (NpgsqlConnection con = new NpgsqlConnection(Configuracion.ConStr))
            {
                //Abrir una conexion
                con.Open();
                //Definir la consulta
                string sql = @"insert into par_usu(id_jug, id_par) 
                               values (@id,
                                      (select id from partida where pass like '' limit 1)) 
                               returning id_par";
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                cmd.Parameters.AddWithValue("@id", usuario.Id);
                int idPar = Convert.ToInt32(cmd.ExecuteScalar());
                return CargarPartidaID(idPar);
            }
        }

        public EPartida CargarPartidaID(int idPar)
        {
            using (NpgsqlConnection con = new NpgsqlConnection(Configuracion.ConStr))
            {
                //Abrir una conexion
                con.Open();
                //Definir la consulta
                string sql = @"select id, cant_jug, jug_actual from partida 
                        where id = @id";
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                cmd.Parameters.AddWithValue("@id", idPar);
                NpgsqlDataReader reader = cmd.ExecuteReader();

                if (reader.Read())
                {
                    return CargarPartida(reader);
                }
            }
            return null;

        }

        private EPartida CargarPartida(NpgsqlDataReader reader)
        {
            return new EPartida
            {
                Id = reader.GetInt32(0),
                CantJugadores = reader.GetInt32(1),
                JugadorActual = reader.GetInt32(2)
            };
        }

        private int Siguiente(EPartida partida)
        {
            using (NpgsqlConnection con = new NpgsqlConnection(Configuracion.ConStr))
            {
                //Abrir una conexion
                con.Open();
                //Definir la consulta
                string sql = @"select id_jug from par_usu
                                where id_par = @idP and orden > (select orden from par_usu where id_jug = @idJ and id_par = @idP)
                                order by orden limit 1";
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                cmd.Parameters.AddWithValue("@idP", partida.Id);
                cmd.Parameters.AddWithValue("@idJ", partida.JugadorActual);
                object o = cmd.ExecuteScalar();
                int id = o == DBNull.Value ? 0 : Convert.ToInt32(o);
                if (id > 0)
                {
                    return id;
                }
                cmd.Parameters.Clear();
                sql = @"select id_jug from par_usu
                                where id_par = @idP 
                                order by orden limit 1";
                cmd = new NpgsqlCommand(sql, con);
                cmd.Parameters.AddWithValue("@idP", partida.Id);
                o = cmd.ExecuteScalar();
                id = o == DBNull.Value ? 0 : Convert.ToInt32(o);
                return id;
            }
        }

        public void Actualizar(EPartida partida)
        {
            using (NpgsqlConnection con = new NpgsqlConnection(Configuracion.ConStr))
            {
                //Abrir una conexion
                con.Open();
                //Definir la consulta
                string sql = @"update partida set jug_actual = @sig where id = @id";
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                cmd.Parameters.AddWithValue("@id", partida.Id);
                cmd.Parameters.AddWithValue("@sig", Siguiente(partida));
                cmd.ExecuteNonQuery();
            }
        }
    }
}



/*
 CREATE TABLE public.par_usu
(
    id serial,
    id_jug integer NOT NULL,
    id_par integer NOT NULL,
    orden serial,
    CONSTRAINT par_usu_pkey PRIMARY KEY (id),
    CONSTRAINT fk_parusu_par FOREIGN KEY (id_jug)
        REFERENCES public.usuario (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)

CREATE TABLE public.partida
(
    id serial,
    pass text COLLATE pg_catalog."default",
    cant_jug integer,
    jug_actual integer,
    CONSTRAINT partida_pkey PRIMARY KEY (id)
)

 */
