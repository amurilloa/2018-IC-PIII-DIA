﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DemoProyecto
{
    public partial class FrmJuego : Form
    {
        public EUsuario Usuario { get; set; }
        private EPartida partida;
        private JuegoBOL log;

        public FrmJuego()
        {
            InitializeComponent();
        }

        private void BuscarPartida(object sender, EventArgs e)
        {
            
            partida = log.BuscarPublica(Usuario);
            button1.Visible = !(partida?.Id > 0);
            groupBox1.Visible = (partida?.Id > 0);
            Refrescar();
            timer1.Start();
        }

        private void Refrescar()
        {
            partida = log.CargarPartida(partida);
            lblEstado.Text = "Jugando .... " + partida.JugadorActual;//CargarUsuarioID( partida.JugadorActual)
            button2.Enabled = partida.JugadorActual == Usuario.Id;
        }

        private void FrmJuego_FormClosing(object sender, FormClosingEventArgs e)
        {
            Owner?.Show();
        }

        private void FrmJuego_Load(object sender, EventArgs e)
        {
            log = new JuegoBOL();
            lblJugador.Text = Usuario.Usuario;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            Refrescar();
        }

        private void Plantarse(object sender, EventArgs e)
        {
            log.Plantarse(partida);
        }
    }
}
