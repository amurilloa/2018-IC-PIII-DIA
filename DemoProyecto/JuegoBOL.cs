﻿using System;

namespace DemoProyecto
{
    internal class JuegoBOL
    {
        public JuegoBOL()
        {
        }

        internal EPartida BuscarPublica(EUsuario usuario)
        {
            return new JuegoDAL().Join(usuario);
        }

        internal EPartida CargarPartida(EPartida partida)
        {
            return new JuegoDAL().CargarPartidaID(partida.Id);
        }

        internal void Plantarse(EPartida partida)
        {
            new JuegoDAL().Actualizar(partida);
        }
    }
}