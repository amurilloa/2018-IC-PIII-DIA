﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DemoProyecto
{
    public partial class Form1 : Form
    {
        private UsuarioBOL ubo;

        public Form1()
        {
            InitializeComponent();
        }

        private void Login(object sender, EventArgs e)
        {
            EUsuario usu = new EUsuario
            {
                Usuario = txtUsuario.Text.Trim(),
                Password = txtPassword.Text.Trim(),
                Email = txtUsuario.Text.Trim()
            };
            usu = ubo.Login(usu);
            if (usu?.Id > 0)
            {
                FrmJuego frm = new FrmJuego
                {
                    Usuario = usu,
                    Owner = this
                };
                Hide();
                frm.Show();
                txtUsuario.Clear();
                txtPassword.Clear();
            }
            else {
                txtPassword.Clear();
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            ubo = new UsuarioBOL();
        }
    }
}
