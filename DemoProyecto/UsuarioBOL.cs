﻿using System;

namespace DemoProyecto
{
    internal class UsuarioBOL
    {
        internal EUsuario Login(EUsuario usu)
        {
            return new UsuarioDAL().Verificar(usu);
        }
    }
}