﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoProyecto
{
    public class UsuarioDAL
    {
        public EUsuario Verificar(EUsuario usuario)
        {
            using (NpgsqlConnection con = new NpgsqlConnection(Configuracion.ConStr))
            {
                //Abrir una conexion
                con.Open();
                //Definir la consulta
                string sql = @"select id, cedula, usuario, 
                               nombre, apellido_uno, apellido_dos, 
                                pass, email, id_foto from usuario where (usuario = @usuario or email = @email)
                                and pass = @pass";
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                cmd.Parameters.AddWithValue("@usuario", usuario.Usuario);
                cmd.Parameters.AddWithValue("@email", usuario.Email);
                cmd.Parameters.AddWithValue("@pass", usuario.Password);

                NpgsqlDataReader reader = cmd.ExecuteReader();

                if (reader.Read())
                {
                    return CargarUsuario(reader);
                }
            }
            return null;
        }


        private EUsuario CargarUsuario(NpgsqlDataReader reader)
        {
            EUsuario usu = new EUsuario
            {
                //No validar la llave primaria, no es necesario, solo de ejemplo xD
                Id = reader["id"] != DBNull.Value ? Convert.ToInt32(reader["id"]) : 0,
                Cedula = reader["cedula"] != DBNull.Value ? reader["cedula"].ToString() : "Indocumentado",
                Usuario = reader["usuario"].ToString(),
                Nombre = reader["nombre"].ToString(),
                ApellidoUno = reader["apellido_uno"].ToString(),
                ApellidoDos = reader["apellido_dos"].ToString(),
                Password = reader["pass"].ToString(),
                Email = reader["email"].ToString()
            };
            return usu;
        }
    }
}
