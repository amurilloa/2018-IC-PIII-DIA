﻿using DemoWebService.BCCR;
using DemoWebService.WebServiceIMN;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace DemoWebService
{
    public partial class FrmTipoCambio : Form
    {
        private double venta;
        private double compra;
        private DateTime fecha;

        public FrmTipoCambio()
        {
            InitializeComponent();
        }

        private void FrmTipoCambio_Load(object sender, EventArgs e)
        {

            WSMeteorologicoClient ws = new WSMeteorologicoClient("WSMeteorologico");
            Console.WriteLine(ws.efemerides(new efemerides()));
            EFEMERIDES efe = ws.efemerides(new efemerides()).ParseXML<EFEMERIDES>();
            Console.WriteLine(efe.FASELUNAR.Value);


            dtpFechaActual.Value = DateTime.Now;
            dtpFechaActual.MaxDate = DateTime.Now;
            CargarTipoCambio();
        }

        private void CargarTipoCambio()
        {
            try
            {
                fecha = dtpFechaActual.Value;

                wsIndicadoresEconomicosSoapClient ws =
                    new wsIndicadoresEconomicosSoapClient("wsIndicadoresEconomicosSoap");
                string fec = fecha.ToString("dd/MM/yyyy");
                string res = ws.ObtenerIndicadoresEconomicosXML("318", fec, fec, "UTN-PrograIII", "N");

                //XmlDocument doc = new XmlDocument();
                //doc.LoadXml(res);
                //Console.WriteLine(res);

                //XmlNodeList list = doc.GetElementsByTagName("NUM_VALOR");
                //venta = Convert.ToDouble(list[0].InnerText);
                //lblVenta.Text = String.Format("Venta: {0:F2}", venta);

                //res = ws.ObtenerIndicadoresEconomicosXML("317", fec, fec, "UTN-PrograIII", "N");

                //doc = new XmlDocument();
                //doc.LoadXml(res);

                //list = doc.GetElementsByTagName("NUM_VALOR");
                //compra = Convert.ToDouble(list[0].InnerText);
                //lblCompra.Text = String.Format("Compra: {0:F2}", compra);
                //txtCRC.Text = String.Format("{0:F2}", venta);

                Datos_de_INGC011_CAT_INDICADORECONOMIC ind = res.ParseXML<Datos_de_INGC011_CAT_INDICADORECONOMIC>();
                venta = ind.INGC011_CAT_INDICADORECONOMIC.NUM_VALOR;
                lblVenta.Text = String.Format("Venta: {0:F2}", venta);
                txtCRC.Text = String.Format("{0:F2}", venta);

                res = ws.ObtenerIndicadoresEconomicosXML("317", fec, fec, "UTN-PrograIII", "N");
                ind = res.ParseXML<Datos_de_INGC011_CAT_INDICADORECONOMIC>();
                compra = ind.INGC011_CAT_INDICADORECONOMIC.NUM_VALOR;
                lblCompra.Text = String.Format("Compra: {0:F2}", compra);
            }
            catch (Exception)
            {
                MessageBox.Show("Sin datos dispobibles para la fecha seleccionada");
            }
        }

        private void ConvertirAUSD(object sender, KeyEventArgs e)
        {
            if (Double.TryParse(txtCRC.Text.Trim(), out double cant))
            {

                txtUSD.Text = String.Format("{0:F2}", cant / venta);
            }
            else
            {
                txtUSD.Text = String.Format("{0:F2}", 0);
            }
        }

        private void ConvertirACRC(object sender, KeyEventArgs e)
        {
            if (Double.TryParse(txtUSD.Text.Trim(), out double cant))
            {

                txtCRC.Text = String.Format("{0:F2}", cant * venta);
            }
            else
            {
                txtCRC.Text = String.Format("{0:F2}", 0);
            }
        }

        private void CambioFecha(object sender, EventArgs e)
        {
            CargarTipoCambio();
        }
    }
}