﻿namespace DemoWebService
{
    partial class FrmTipoCambio
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblCompra = new System.Windows.Forms.Label();
            this.lblVenta = new System.Windows.Forms.Label();
            this.txtCRC = new System.Windows.Forms.TextBox();
            this.txtUSD = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.dtpFechaActual = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lblCompra
            // 
            this.lblCompra.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblCompra.AutoSize = true;
            this.lblCompra.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCompra.Location = new System.Drawing.Point(725, 24);
            this.lblCompra.Name = "lblCompra";
            this.lblCompra.Size = new System.Drawing.Size(261, 37);
            this.lblCompra.TabIndex = 0;
            this.lblCompra.Text = "Compra:  {0:F2}";
            // 
            // lblVenta
            // 
            this.lblVenta.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblVenta.AutoSize = true;
            this.lblVenta.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVenta.Location = new System.Drawing.Point(753, 71);
            this.lblVenta.Name = "lblVenta";
            this.lblVenta.Size = new System.Drawing.Size(229, 37);
            this.lblVenta.TabIndex = 1;
            this.lblVenta.Text = "Venta:  {0:F2}";
            // 
            // txtCRC
            // 
            this.txtCRC.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCRC.Location = new System.Drawing.Point(158, 261);
            this.txtCRC.Name = "txtCRC";
            this.txtCRC.Size = new System.Drawing.Size(366, 116);
            this.txtCRC.TabIndex = 2;
            this.txtCRC.Text = "568.34";
            this.txtCRC.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtCRC.KeyUp += new System.Windows.Forms.KeyEventHandler(this.ConvertirAUSD);
            // 
            // txtUSD
            // 
            this.txtUSD.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUSD.Location = new System.Drawing.Point(158, 404);
            this.txtUSD.Name = "txtUSD";
            this.txtUSD.Size = new System.Drawing.Size(366, 116);
            this.txtUSD.TabIndex = 3;
            this.txtUSD.Text = "1.00";
            this.txtUSD.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtUSD.KeyUp += new System.Windows.Forms.KeyEventHandler(this.ConvertirACRC);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(541, 264);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(255, 108);
            this.label1.TabIndex = 4;
            this.label1.Text = "CRC";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(541, 407);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(250, 108);
            this.label2.TabIndex = 5;
            this.label2.Text = "USD";
            // 
            // dtpFechaActual
            // 
            this.dtpFechaActual.CustomFormat = "dd/MM/yyyy";
            this.dtpFechaActual.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpFechaActual.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpFechaActual.Location = new System.Drawing.Point(46, 71);
            this.dtpFechaActual.MinDate = new System.DateTime(1988, 1, 1, 0, 0, 0, 0);
            this.dtpFechaActual.Name = "dtpFechaActual";
            this.dtpFechaActual.Size = new System.Drawing.Size(229, 44);
            this.dtpFechaActual.TabIndex = 6;
            this.dtpFechaActual.TabStop = false;
            this.dtpFechaActual.Validated += new System.EventHandler(this.CambioFecha);
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(12, 25);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(303, 37);
            this.label3.TabIndex = 7;
            this.label3.Text = "Fecha de Consulta";
            // 
            // FrmTipoCambio
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(998, 697);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.dtpFechaActual);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtUSD);
            this.Controls.Add(this.txtCRC);
            this.Controls.Add(this.lblVenta);
            this.Controls.Add(this.lblCompra);
            this.MinimumSize = new System.Drawing.Size(1024, 768);
            this.Name = "FrmTipoCambio";
            this.Text = "Tipo Cambio";
            this.Load += new System.EventHandler(this.FrmTipoCambio_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblCompra;
        private System.Windows.Forms.Label lblVenta;
        private System.Windows.Forms.TextBox txtCRC;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtUSD;
        private System.Windows.Forms.DateTimePicker dtpFechaActual;
        private System.Windows.Forms.Label label3;
    }
}