﻿namespace DemoWebService
{
    partial class FrmPronostico
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lblSolPone = new System.Windows.Forms.Label();
            this.lblSolSale = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.lblLunaPone = new System.Windows.Forms.Label();
            this.lblLunaSale = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.lblFaseLunar = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.pnlManana = new System.Windows.Forms.Panel();
            this.lblComentarioM = new System.Windows.Forms.TextBox();
            this.lblTituloM = new System.Windows.Forms.Label();
            this.pbxImagenM = new System.Windows.Forms.PictureBox();
            this.pnlTarde = new System.Windows.Forms.Panel();
            this.lblComentarioT = new System.Windows.Forms.TextBox();
            this.lblTituloT = new System.Windows.Forms.Label();
            this.pbxImagenT = new System.Windows.Forms.PictureBox();
            this.pnlNoche = new System.Windows.Forms.Panel();
            this.lblComentarioN = new System.Windows.Forms.TextBox();
            this.lblTituloN = new System.Windows.Forms.Label();
            this.pbxImagenN = new System.Windows.Forms.PictureBox();
            this.cbxRegiones = new System.Windows.Forms.ComboBox();
            this.pRONOSTICOREGIONALREGIONBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.pRONOSTICOREGIONALREGIONCIUDADBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblCCM = new System.Windows.Forms.TextBox();
            this.lblCTM = new System.Windows.Forms.Label();
            this.pbxCM = new System.Windows.Forms.PictureBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.lblCCT = new System.Windows.Forms.TextBox();
            this.lblCTT = new System.Windows.Forms.Label();
            this.pbxCT = new System.Windows.Forms.PictureBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.lblCCN = new System.Windows.Forms.TextBox();
            this.lblCTN = new System.Windows.Forms.Label();
            this.pbxCN = new System.Windows.Forms.PictureBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.pnlManana.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxImagenM)).BeginInit();
            this.pnlTarde.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxImagenT)).BeginInit();
            this.pnlNoche.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxImagenN)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pRONOSTICOREGIONALREGIONBindingSource)).BeginInit();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pRONOSTICOREGIONALREGIONCIUDADBindingSource)).BeginInit();
            this.flowLayoutPanel2.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxCM)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxCT)).BeginInit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxCN)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lblSolPone);
            this.groupBox1.Controls.Add(this.lblSolSale);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(22, 676);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(295, 175);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Sol:";
            // 
            // lblSolPone
            // 
            this.lblSolPone.AutoSize = true;
            this.lblSolPone.Location = new System.Drawing.Point(13, 123);
            this.lblSolPone.Name = "lblSolPone";
            this.lblSolPone.Size = new System.Drawing.Size(100, 37);
            this.lblSolPone.TabIndex = 1;
            this.lblSolPone.Text = "Pone:";
            // 
            // lblSolSale
            // 
            this.lblSolSale.AutoSize = true;
            this.lblSolSale.Location = new System.Drawing.Point(15, 60);
            this.lblSolSale.Name = "lblSolSale";
            this.lblSolSale.Size = new System.Drawing.Size(89, 37);
            this.lblSolSale.TabIndex = 0;
            this.lblSolSale.Text = "Sale:";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.lblLunaPone);
            this.groupBox2.Controls.Add(this.lblLunaSale);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(323, 676);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(281, 175);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Luna:";
            // 
            // lblLunaPone
            // 
            this.lblLunaPone.AutoSize = true;
            this.lblLunaPone.Location = new System.Drawing.Point(13, 123);
            this.lblLunaPone.Name = "lblLunaPone";
            this.lblLunaPone.Size = new System.Drawing.Size(100, 37);
            this.lblLunaPone.TabIndex = 1;
            this.lblLunaPone.Text = "Pone:";
            // 
            // lblLunaSale
            // 
            this.lblLunaSale.AutoSize = true;
            this.lblLunaSale.Location = new System.Drawing.Point(15, 60);
            this.lblLunaSale.Name = "lblLunaSale";
            this.lblLunaSale.Size = new System.Drawing.Size(98, 37);
            this.lblLunaSale.TabIndex = 0;
            this.lblLunaSale.Text = "Sale: ";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.lblFaseLunar);
            this.groupBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox3.Location = new System.Drawing.Point(610, 676);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(644, 175);
            this.groupBox3.TabIndex = 3;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Fase Lunar";
            // 
            // lblFaseLunar
            // 
            this.lblFaseLunar.AutoSize = true;
            this.lblFaseLunar.Location = new System.Drawing.Point(18, 60);
            this.lblFaseLunar.Name = "lblFaseLunar";
            this.lblFaseLunar.Size = new System.Drawing.Size(98, 37);
            this.lblFaseLunar.TabIndex = 1;
            this.lblFaseLunar.Text = "Sale: ";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.flowLayoutPanel1);
            this.groupBox4.Controls.Add(this.cbxRegiones);
            this.groupBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox4.Location = new System.Drawing.Point(22, 22);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(582, 620);
            this.groupBox4.TabIndex = 2;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Pronóstico Regional";
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.pnlManana);
            this.flowLayoutPanel1.Controls.Add(this.pnlTarde);
            this.flowLayoutPanel1.Controls.Add(this.pnlNoche);
            this.flowLayoutPanel1.Location = new System.Drawing.Point(31, 123);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(525, 464);
            this.flowLayoutPanel1.TabIndex = 1;
            // 
            // pnlManana
            // 
            this.pnlManana.Controls.Add(this.lblComentarioM);
            this.pnlManana.Controls.Add(this.lblTituloM);
            this.pnlManana.Controls.Add(this.pbxImagenM);
            this.pnlManana.Location = new System.Drawing.Point(3, 3);
            this.pnlManana.Name = "pnlManana";
            this.pnlManana.Size = new System.Drawing.Size(522, 149);
            this.pnlManana.TabIndex = 0;
            // 
            // lblComentarioM
            // 
            this.lblComentarioM.BackColor = System.Drawing.SystemColors.Control;
            this.lblComentarioM.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lblComentarioM.Enabled = false;
            this.lblComentarioM.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.875F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblComentarioM.Location = new System.Drawing.Point(151, 62);
            this.lblComentarioM.Multiline = true;
            this.lblComentarioM.Name = "lblComentarioM";
            this.lblComentarioM.ReadOnly = true;
            this.lblComentarioM.Size = new System.Drawing.Size(355, 74);
            this.lblComentarioM.TabIndex = 2;
            this.lblComentarioM.Text = "Comentario";
            // 
            // lblTituloM
            // 
            this.lblTituloM.AutoSize = true;
            this.lblTituloM.Location = new System.Drawing.Point(142, 22);
            this.lblTituloM.Name = "lblTituloM";
            this.lblTituloM.Size = new System.Drawing.Size(96, 37);
            this.lblTituloM.TabIndex = 1;
            this.lblTituloM.Text = "Título";
            // 
            // pbxImagenM
            // 
            this.pbxImagenM.Location = new System.Drawing.Point(3, 6);
            this.pbxImagenM.Name = "pbxImagenM";
            this.pbxImagenM.Size = new System.Drawing.Size(133, 133);
            this.pbxImagenM.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbxImagenM.TabIndex = 0;
            this.pbxImagenM.TabStop = false;
            // 
            // pnlTarde
            // 
            this.pnlTarde.Controls.Add(this.lblComentarioT);
            this.pnlTarde.Controls.Add(this.lblTituloT);
            this.pnlTarde.Controls.Add(this.pbxImagenT);
            this.pnlTarde.Location = new System.Drawing.Point(3, 158);
            this.pnlTarde.Name = "pnlTarde";
            this.pnlTarde.Size = new System.Drawing.Size(522, 149);
            this.pnlTarde.TabIndex = 1;
            // 
            // lblComentarioT
            // 
            this.lblComentarioT.BackColor = System.Drawing.SystemColors.Control;
            this.lblComentarioT.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lblComentarioT.Enabled = false;
            this.lblComentarioT.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.875F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblComentarioT.Location = new System.Drawing.Point(151, 66);
            this.lblComentarioT.Multiline = true;
            this.lblComentarioT.Name = "lblComentarioT";
            this.lblComentarioT.ReadOnly = true;
            this.lblComentarioT.Size = new System.Drawing.Size(355, 74);
            this.lblComentarioT.TabIndex = 2;
            this.lblComentarioT.Text = "Comentario";
            // 
            // lblTituloT
            // 
            this.lblTituloT.AutoSize = true;
            this.lblTituloT.Location = new System.Drawing.Point(148, 15);
            this.lblTituloT.Name = "lblTituloT";
            this.lblTituloT.Size = new System.Drawing.Size(96, 37);
            this.lblTituloT.TabIndex = 1;
            this.lblTituloT.Text = "Título";
            // 
            // pbxImagenT
            // 
            this.pbxImagenT.Location = new System.Drawing.Point(3, 7);
            this.pbxImagenT.Name = "pbxImagenT";
            this.pbxImagenT.Size = new System.Drawing.Size(133, 133);
            this.pbxImagenT.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbxImagenT.TabIndex = 0;
            this.pbxImagenT.TabStop = false;
            // 
            // pnlNoche
            // 
            this.pnlNoche.Controls.Add(this.lblComentarioN);
            this.pnlNoche.Controls.Add(this.lblTituloN);
            this.pnlNoche.Controls.Add(this.pbxImagenN);
            this.pnlNoche.Location = new System.Drawing.Point(3, 313);
            this.pnlNoche.Name = "pnlNoche";
            this.pnlNoche.Size = new System.Drawing.Size(522, 149);
            this.pnlNoche.TabIndex = 2;
            // 
            // lblComentarioN
            // 
            this.lblComentarioN.BackColor = System.Drawing.SystemColors.Control;
            this.lblComentarioN.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lblComentarioN.Enabled = false;
            this.lblComentarioN.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.875F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblComentarioN.Location = new System.Drawing.Point(157, 62);
            this.lblComentarioN.Multiline = true;
            this.lblComentarioN.Name = "lblComentarioN";
            this.lblComentarioN.ReadOnly = true;
            this.lblComentarioN.Size = new System.Drawing.Size(349, 74);
            this.lblComentarioN.TabIndex = 2;
            this.lblComentarioN.Text = "Comentario";
            // 
            // lblTituloN
            // 
            this.lblTituloN.AutoSize = true;
            this.lblTituloN.Location = new System.Drawing.Point(147, 15);
            this.lblTituloN.Name = "lblTituloN";
            this.lblTituloN.Size = new System.Drawing.Size(96, 37);
            this.lblTituloN.TabIndex = 1;
            this.lblTituloN.Text = "Título";
            // 
            // pbxImagenN
            // 
            this.pbxImagenN.Location = new System.Drawing.Point(3, 3);
            this.pbxImagenN.Name = "pbxImagenN";
            this.pbxImagenN.Size = new System.Drawing.Size(133, 133);
            this.pbxImagenN.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbxImagenN.TabIndex = 0;
            this.pbxImagenN.TabStop = false;
            // 
            // cbxRegiones
            // 
            this.cbxRegiones.DataSource = this.pRONOSTICOREGIONALREGIONBindingSource;
            this.cbxRegiones.DisplayMember = "nombre";
            this.cbxRegiones.FormattingEnabled = true;
            this.cbxRegiones.Location = new System.Drawing.Point(31, 57);
            this.cbxRegiones.Name = "cbxRegiones";
            this.cbxRegiones.Size = new System.Drawing.Size(525, 45);
            this.cbxRegiones.TabIndex = 0;
            this.cbxRegiones.ValueMember = "idRegion";
            this.cbxRegiones.SelectedIndexChanged += new System.EventHandler(this.SeleccionRegion);
            // 
            // pRONOSTICOREGIONALREGIONBindingSource
            // 
            this.pRONOSTICOREGIONALREGIONBindingSource.DataSource = typeof(DemoWebService.PRONOSTICO_REGIONALREGION);
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.flowLayoutPanel2);
            this.groupBox5.Controls.Add(this.comboBox1);
            this.groupBox5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox5.Location = new System.Drawing.Point(625, 22);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(582, 620);
            this.groupBox5.TabIndex = 3;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Pronóstico Ciudad";
            // 
            // comboBox1
            // 
            this.comboBox1.DataSource = this.pRONOSTICOREGIONALREGIONCIUDADBindingSource;
            this.comboBox1.DisplayMember = "nombre";
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(25, 57);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(525, 45);
            this.comboBox1.TabIndex = 1;
            this.comboBox1.ValueMember = "id";
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.SeleccionCiudad);
            // 
            // pRONOSTICOREGIONALREGIONCIUDADBindingSource
            // 
            this.pRONOSTICOREGIONALREGIONCIUDADBindingSource.DataSource = typeof(DemoWebService.PRONOSTICO_REGIONALREGIONCIUDAD);
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.Controls.Add(this.panel1);
            this.flowLayoutPanel2.Controls.Add(this.panel2);
            this.flowLayoutPanel2.Controls.Add(this.panel3);
            this.flowLayoutPanel2.Location = new System.Drawing.Point(25, 123);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(525, 464);
            this.flowLayoutPanel2.TabIndex = 2;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.lblCCM);
            this.panel1.Controls.Add(this.lblCTM);
            this.panel1.Controls.Add(this.pbxCM);
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(522, 149);
            this.panel1.TabIndex = 0;
            // 
            // lblCCM
            // 
            this.lblCCM.BackColor = System.Drawing.SystemColors.Control;
            this.lblCCM.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lblCCM.Enabled = false;
            this.lblCCM.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.875F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCCM.Location = new System.Drawing.Point(151, 62);
            this.lblCCM.Multiline = true;
            this.lblCCM.Name = "lblCCM";
            this.lblCCM.ReadOnly = true;
            this.lblCCM.Size = new System.Drawing.Size(355, 74);
            this.lblCCM.TabIndex = 2;
            this.lblCCM.Text = "Comentario";
            // 
            // lblCTM
            // 
            this.lblCTM.AutoSize = true;
            this.lblCTM.Location = new System.Drawing.Point(142, 22);
            this.lblCTM.Name = "lblCTM";
            this.lblCTM.Size = new System.Drawing.Size(96, 37);
            this.lblCTM.TabIndex = 1;
            this.lblCTM.Text = "Título";
            // 
            // pbxCM
            // 
            this.pbxCM.Location = new System.Drawing.Point(3, 6);
            this.pbxCM.Name = "pbxCM";
            this.pbxCM.Size = new System.Drawing.Size(133, 133);
            this.pbxCM.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbxCM.TabIndex = 0;
            this.pbxCM.TabStop = false;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.lblCCT);
            this.panel2.Controls.Add(this.lblCTT);
            this.panel2.Controls.Add(this.pbxCT);
            this.panel2.Location = new System.Drawing.Point(3, 158);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(522, 149);
            this.panel2.TabIndex = 1;
            // 
            // lblCCT
            // 
            this.lblCCT.BackColor = System.Drawing.SystemColors.Control;
            this.lblCCT.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lblCCT.Enabled = false;
            this.lblCCT.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.875F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCCT.Location = new System.Drawing.Point(151, 66);
            this.lblCCT.Multiline = true;
            this.lblCCT.Name = "lblCCT";
            this.lblCCT.ReadOnly = true;
            this.lblCCT.Size = new System.Drawing.Size(355, 74);
            this.lblCCT.TabIndex = 2;
            this.lblCCT.Text = "Comentario";
            // 
            // lblCTT
            // 
            this.lblCTT.AutoSize = true;
            this.lblCTT.Location = new System.Drawing.Point(148, 15);
            this.lblCTT.Name = "lblCTT";
            this.lblCTT.Size = new System.Drawing.Size(96, 37);
            this.lblCTT.TabIndex = 1;
            this.lblCTT.Text = "Título";
            // 
            // pbxCT
            // 
            this.pbxCT.Location = new System.Drawing.Point(3, 7);
            this.pbxCT.Name = "pbxCT";
            this.pbxCT.Size = new System.Drawing.Size(133, 133);
            this.pbxCT.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbxCT.TabIndex = 0;
            this.pbxCT.TabStop = false;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.lblCCN);
            this.panel3.Controls.Add(this.lblCTN);
            this.panel3.Controls.Add(this.pbxCN);
            this.panel3.Location = new System.Drawing.Point(3, 313);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(522, 149);
            this.panel3.TabIndex = 2;
            // 
            // lblCCN
            // 
            this.lblCCN.BackColor = System.Drawing.SystemColors.Control;
            this.lblCCN.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lblCCN.Enabled = false;
            this.lblCCN.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.875F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCCN.Location = new System.Drawing.Point(157, 62);
            this.lblCCN.Multiline = true;
            this.lblCCN.Name = "lblCCN";
            this.lblCCN.ReadOnly = true;
            this.lblCCN.Size = new System.Drawing.Size(349, 74);
            this.lblCCN.TabIndex = 2;
            this.lblCCN.Text = "Comentario";
            // 
            // lblCTN
            // 
            this.lblCTN.AutoSize = true;
            this.lblCTN.Location = new System.Drawing.Point(147, 15);
            this.lblCTN.Name = "lblCTN";
            this.lblCTN.Size = new System.Drawing.Size(96, 37);
            this.lblCTN.TabIndex = 1;
            this.lblCTN.Text = "Título";
            // 
            // pbxCN
            // 
            this.pbxCN.Location = new System.Drawing.Point(3, 3);
            this.pbxCN.Name = "pbxCN";
            this.pbxCN.Size = new System.Drawing.Size(133, 133);
            this.pbxCN.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbxCN.TabIndex = 0;
            this.pbxCN.TabStop = false;
            // 
            // FrmPronostico
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1264, 863);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "FrmPronostico";
            this.Text = "Pronóstico";
            this.Load += new System.EventHandler(this.FrmPronostico_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.flowLayoutPanel1.ResumeLayout(false);
            this.pnlManana.ResumeLayout(false);
            this.pnlManana.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxImagenM)).EndInit();
            this.pnlTarde.ResumeLayout(false);
            this.pnlTarde.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxImagenT)).EndInit();
            this.pnlNoche.ResumeLayout(false);
            this.pnlNoche.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxImagenN)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pRONOSTICOREGIONALREGIONBindingSource)).EndInit();
            this.groupBox5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pRONOSTICOREGIONALREGIONCIUDADBindingSource)).EndInit();
            this.flowLayoutPanel2.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxCM)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxCT)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxCN)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label lblSolPone;
        private System.Windows.Forms.Label lblSolSale;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label lblLunaPone;
        private System.Windows.Forms.Label lblLunaSale;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label lblFaseLunar;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.ComboBox cbxRegiones;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.BindingSource pRONOSTICOREGIONALREGIONBindingSource;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Panel pnlManana;
        private System.Windows.Forms.TextBox lblComentarioM;
        private System.Windows.Forms.Label lblTituloM;
        private System.Windows.Forms.PictureBox pbxImagenM;
        private System.Windows.Forms.Panel pnlTarde;
        private System.Windows.Forms.TextBox lblComentarioT;
        private System.Windows.Forms.Label lblTituloT;
        private System.Windows.Forms.PictureBox pbxImagenT;
        private System.Windows.Forms.Panel pnlNoche;
        private System.Windows.Forms.TextBox lblComentarioN;
        private System.Windows.Forms.Label lblTituloN;
        private System.Windows.Forms.PictureBox pbxImagenN;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.BindingSource pRONOSTICOREGIONALREGIONCIUDADBindingSource;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox lblCCM;
        private System.Windows.Forms.Label lblCTM;
        private System.Windows.Forms.PictureBox pbxCM;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TextBox lblCCT;
        private System.Windows.Forms.Label lblCTT;
        private System.Windows.Forms.PictureBox pbxCT;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.TextBox lblCCN;
        private System.Windows.Forms.Label lblCTN;
        private System.Windows.Forms.PictureBox pbxCN;
    }
}