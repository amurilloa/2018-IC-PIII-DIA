﻿using DemoWebService.WebServiceIMN;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DemoWebService
{
    public partial class FrmPronostico : Form
    {
        public FrmPronostico()
        {
            InitializeComponent();
        }

        private void FrmPronostico_Load(object sender, EventArgs e)
        {
            CargarEfemerides();
            CargarRegiones();

        }

        private void CargarRegiones()
        {
            WSMeteorologicoClient ws = new WSMeteorologicoClient("WSMeteorologico");
            PRONOSTICO_REGIONAL reg = ws.pronosticoRegional(new pronosticoRegion()).ParseXML<PRONOSTICO_REGIONAL>();
            cbxRegiones.DataSource = reg.REGIONES;
        }

        private void CargarEfemerides()
        {
            WSMeteorologicoClient ws = new WSMeteorologicoClient("WSMeteorologico");
            EFEMERIDES ef = ws.efemerides(new efemerides()).ParseXML<EFEMERIDES>();
            lblSolSale.Text = String.Format("Sale: {0}", ef.EFEMERIDE_SOL.SALE);
            lblSolPone.Text = String.Format("Pone: {0}", ef.EFEMERIDE_SOL.SEPONE);

            lblLunaSale.Text = String.Format("Sale: {0}", ef.EFEMERIDE_LUNA.SALE);
            lblLunaPone.Text = String.Format("Pone: {0}", ef.EFEMERIDE_LUNA.SEPONE);

            lblFaseLunar.Text = ef.FASELUNAR.Value;
        }

        private void SeleccionRegion(object sender, EventArgs e)
        {
            WSMeteorologicoClient ws = new WSMeteorologicoClient("WSMeteorologico");
            if (cbxRegiones.SelectedIndex >= 0)
            {
                int id = (cbxRegiones.SelectedItem as PRONOSTICO_REGIONALREGION)?.idRegion ?? -1;
                if (id != -1)
                {
                    pnlManana.Hide();
                    pnlTarde.Hide();
                    pnlNoche.Hide();
                    PRONOSTICO_REGIONAL reg = ws.pronosticoRegionalxID(id).ParseXML<PRONOSTICO_REGIONAL>();
                    if (reg.REGIONES[0].ESTADOMANANA != null)
                    {
                        pbxImagenM.ImageLocation = String.Format("https://www.imn.ac.cr{0}", reg.REGIONES[0].ESTADOMANANA.imgPath);
                        lblComentarioM.Text = reg.REGIONES[0].COMENTARIOMANANA;
                        lblTituloM.Text = reg.REGIONES[0].ESTADOMANANA.Value;
                        pnlManana.Show();
                    }

                    if (reg.REGIONES[0].ESTADOTARDE != null)
                    {
                        pbxImagenT.ImageLocation = String.Format("https://www.imn.ac.cr{0}", reg.REGIONES[0].ESTADOTARDE.imgPath);
                        lblComentarioT.Text = reg.REGIONES[0].COMENTARIOTARDE;
                        lblTituloT.Text = reg.REGIONES[0].ESTADOTARDE.Value;
                        pnlTarde.Show();
                    }
                    if (reg.REGIONES[0].ESTADONOCHE != null)
                    {
                        pbxImagenN.ImageLocation = String.Format("https://www.imn.ac.cr{0}", reg.REGIONES[0].ESTADONOCHE.imgPath);
                        lblComentarioN.Text = reg.REGIONES[0].COMENTARIONOCHE;
                        lblTituloN.Text = reg.REGIONES[0].ESTADONOCHE.Value;
                        pnlNoche.Show();
                    }
                    CargarCiudades(reg.REGIONES[0].CIUDADES);
                }
            }
        }

        private void CargarCiudades(PRONOSTICO_REGIONALREGIONCIUDAD[] ciudades)
        {
            comboBox1.DataSource = ciudades;
        }

        private void SeleccionCiudad(object sender, EventArgs e)
        {
            PRONOSTICO_REGIONALREGIONCIUDAD ciudad = comboBox1.SelectedItem as PRONOSTICO_REGIONALREGIONCIUDAD;
            panel1.Hide();
            panel2.Hide();
            panel3.Hide();
            if (ciudad.ESTADOMANANA != null) {
                pbxCM.ImageLocation = String.Format("https://www.imn.ac.cr{0}", ciudad.ESTADOMANANA.imgPath);
                lblCCM.Text = ciudad.COMENTARIOMANANA;
                lblCTM.Text = ciudad.ESTADOMANANA.Value;
                panel1.Show();
            }

            if (ciudad.ESTADOTARDE != null)
            {
                pbxCT.ImageLocation = String.Format("https://www.imn.ac.cr{0}", ciudad.ESTADOTARDE.imgPath);
                lblCCT.Text = ciudad.COMENTARIOTARDE;
                lblCTT.Text = ciudad.ESTADOTARDE.Value;
                panel2.Show();
            }

            if (ciudad.ESTADONOCHE != null)
            {
                pbxCN.ImageLocation = String.Format("https://www.imn.ac.cr{0}", ciudad.ESTADONOCHE.imgPath);
                lblCCN.Text = ciudad.COMENTARIONOCHE;
                lblCTN.Text = ciudad.ESTADONOCHE.Value;
                panel3.Show();
            }
        }
    }

}
