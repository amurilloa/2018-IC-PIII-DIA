﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DemoCapasENL;
using Npgsql;

namespace DemoCapasDAL
{
    public class UsuarioDAL
    {
        public List<EUsuario> CargarTodo(string filtro)
        {
            List<EUsuario> usuarios = new List<EUsuario>();
            using (NpgsqlConnection con = new NpgsqlConnection(Configuracion.ConStr))
            {
                //Abrir una conexion
                con.Open();
                //Definir la consulta
                string sql = @"select id, cedula, usuario, 
	                            nombre, apellido_uno, apellido_dos, 
		                            pass, email, id_foto from usuario where
		                            lower(cedula) like @filtro
		                            or lower(nombre) like  @filtro
		                            or lower(usuario) like  @filtro
		                            or lower(apellido_uno) like  @filtro
		                            or lower(email) like  @filtro";
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                cmd.Parameters.AddWithValue("@filtro", filtro.ToLower() + "%");
                NpgsqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    usuarios.Add(CargarUsuario(reader));
                }

            }

            return usuarios;
        }

        public EUsuario Verificar(EUsuario usuario)
        {
            using (NpgsqlConnection con = new NpgsqlConnection(Configuracion.ConStr))
            {
                //Abrir una conexion
                con.Open();
                //Definir la consulta
                string sql = @"select id, cedula, usuario, 
                               nombre, apellido_uno, apellido_dos, 
                                pass, email, id_foto from usuario where (usuario = @usuario or email = @email)
                                and pass = @pass";
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                cmd.Parameters.AddWithValue("@usuario", usuario.Usuario);
                cmd.Parameters.AddWithValue("@email", usuario.Email);
                cmd.Parameters.AddWithValue("@pass", usuario.Password);

                NpgsqlDataReader reader = cmd.ExecuteReader();

                if (reader.Read())
                {
                    return CargarUsuario(reader);
                }
            }
            return null;
        }

        public bool Modificar(EUsuario usuario)
        {
            using (NpgsqlConnection con = new NpgsqlConnection(Configuracion.ConStr))
            {
                con.Open();
                string sql = @"UPDATE usuario
	                            SET cedula=@ced, usuario=@usu, nombre=@nom, 
	                                apellido_uno=@apu, apellido_dos=@aps, 
		                            pass=@pas, email=@ema, id_foto = @fot
	                            WHERE id=@id";
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                cmd.Parameters.AddWithValue("@ced", usuario.Cedula);
                cmd.Parameters.AddWithValue("@usu", usuario.Usuario);
                cmd.Parameters.AddWithValue("@nom", usuario.Nombre);
                cmd.Parameters.AddWithValue("@apu", usuario.ApellidoUno);
                cmd.Parameters.AddWithValue("@aps", usuario.ApellidoDos);
                cmd.Parameters.AddWithValue("@pas", usuario.Password);
                cmd.Parameters.AddWithValue("@ema", usuario.Email);
                cmd.Parameters.AddWithValue("@id", usuario.Id);
                FotoDAL fdal = new FotoDAL();
                if (usuario.Foto != null && usuario.Foto.Id == 0)
                {
                    usuario.Foto = fdal.Insertar(usuario.Foto);
                    cmd.Parameters.AddWithValue("@fot", usuario.Foto.Id);
                }
                else if (usuario.Foto != null)
                {
                    fdal.Modificar(usuario.Foto);
                    cmd.Parameters.AddWithValue("@fot", usuario.Foto.Id);

                }
                else
                {
                    cmd.Parameters.AddWithValue("@fot", DBNull.Value);
                }
                return cmd.ExecuteNonQuery() > 0;
            }
        }

        public void Eliminar(EUsuario usuario)
        {
            NpgsqlTransaction tr = null;
            NpgsqlConnection con = null;
            try
            {
                con = new NpgsqlConnection(Configuracion.ConStr);
                con.Open();
                tr = con.BeginTransaction(); //Inicia la transacción
                string sql = @"DELETE FROM usuario WHERE id = @id";
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                cmd.Transaction = tr;
                cmd.Parameters.AddWithValue("@id", usuario.Id);
                cmd.ExecuteNonQuery();
                if (usuario.Foto != null && usuario.Foto.Id > 0)
                {
                    FotoDAL fdal = new FotoDAL();
                    fdal.Eliminar(usuario.Foto.Id, cmd);
                }
                tr.Commit();
            }
            catch (Exception)
            {
                if (tr != null) { tr.Rollback(); };
                throw;
            }
            finally
            {
                if (con != null) { con.Close(); };
            }
        }

        public bool Insertar(EUsuario usuario)
        {
            using (NpgsqlConnection con = new NpgsqlConnection(Configuracion.ConStr))
            {
                con.Open();
                string sql = @"INSERT INTO usuario(cedula, usuario, nombre,
                                apellido_uno, apellido_dos, pass, email, id_foto)
	                            VALUES (@ced, @usu, @nom, @apu, 
	                            @aps, @pas, @ema, @fot);";
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                cmd.Parameters.AddWithValue("@ced", usuario.Cedula);
                cmd.Parameters.AddWithValue("@usu", usuario.Usuario);
                cmd.Parameters.AddWithValue("@nom", usuario.Nombre);
                cmd.Parameters.AddWithValue("@apu", usuario.ApellidoUno);
                cmd.Parameters.AddWithValue("@aps", usuario.ApellidoDos);
                cmd.Parameters.AddWithValue("@pas", usuario.Password);
                cmd.Parameters.AddWithValue("@ema", usuario.Email);

                if (usuario.Foto != null)
                {
                    FotoDAL fdal = new FotoDAL();
                    usuario.Foto = fdal.Insertar(usuario.Foto);
                    cmd.Parameters.AddWithValue("@fot", usuario.Foto.Id);

                }
                else
                {
                    cmd.Parameters.AddWithValue("@fot", DBNull.Value);
                }

                return cmd.ExecuteNonQuery() > 0;
            }
        }

        private EUsuario CargarUsuario(NpgsqlDataReader reader)
        {
            FotoDAL fdal = new FotoDAL();
            EUsuario usu = new EUsuario
            {
                //No validar la llave primaria, no es necesario, solo de ejemplo xD
                Id = reader["id"] != DBNull.Value ? Convert.ToInt32(reader["id"]) : 0,
                Cedula = reader["cedula"] != DBNull.Value ? reader["cedula"].ToString() : "Indocumentado",
                Usuario = reader["usuario"].ToString(),
                Nombre = reader["nombre"].ToString(),
                ApellidoUno = reader["apellido_uno"].ToString(),
                ApellidoDos = reader["apellido_dos"].ToString(),
                Password = reader["pass"].ToString(),
                Email = reader["email"].ToString(),
                Foto = reader["id_foto"] != DBNull.Value ? fdal.CargarPorId(Convert.ToInt32(reader["id_foto"])) : null
            };
            return usu;
        }
    }
}
