﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using DemoCapasENL;
using Npgsql;

namespace DemoCapasDAL
{
    public class FotoDAL
    {
        public EFoto Insertar(EFoto foto)
        {
            using (NpgsqlConnection con = new NpgsqlConnection(Configuracion.ConStr))
            {
                con.Open();
                string sql = @"INSERT INTO foto(foto)
	                            VALUES (@fot) returning id";
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);

                MemoryStream stream = new MemoryStream();
                foto.Imagen.Save(stream, ImageFormat.Jpeg);
                byte[] pic = stream.ToArray();
                cmd.Parameters.AddWithValue("@fot", pic);
                foto.Id = Convert.ToInt32(cmd.ExecuteScalar());
                return foto;
            }
        }

        public EFoto CargarPorId(int idFoto)
        {
            using (NpgsqlConnection con = new NpgsqlConnection(Configuracion.ConStr))
            {
                //Abrir una conexion
                con.Open();
                //Definir la consulta
                string sql = @"select id, foto from foto where id = @id";
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                cmd.Parameters.AddWithValue("@id", idFoto);
                NpgsqlDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    return CargarFoto(reader);
                }
            }
            return null;
        }

        private EFoto CargarFoto(NpgsqlDataReader reader)
        {

            byte[] f = new byte[0];
            f = (byte[])reader["foto"];
            MemoryStream stream = new MemoryStream(f);

            EFoto foto = new EFoto
            {
                Id = Convert.ToInt32(reader["id"]),
                Imagen = Image.FromStream(stream)
            };
            return foto;
        }

        public void Modificar(EFoto foto)
        {
            using (NpgsqlConnection con = new NpgsqlConnection(Configuracion.ConStr))
            {
                con.Open();
                string sql = @"UPDATE foto
	                            SET foto=@fot
	                            WHERE id=@id";
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);

                MemoryStream stream = new MemoryStream();
                foto.Imagen.Save(stream, ImageFormat.Jpeg);
                byte[] pic = stream.ToArray();
                cmd.Parameters.AddWithValue("@fot", pic);

                cmd.Parameters.AddWithValue("@id", foto.Id);
                cmd.ExecuteNonQuery();
            }
        }

        public void Eliminar(int id, NpgsqlCommand cmd = null)
        {
            string sql = @"DELETE FROM foto WHERE id = @id";
            if (cmd == null)
            {
                using (NpgsqlConnection con = new NpgsqlConnection(Configuracion.ConStr))
                {
                    con.Open();
                    cmd = new NpgsqlCommand(sql, con);
                    cmd.Parameters.AddWithValue("@id", id);
                    cmd.ExecuteNonQuery();
                }
            }
            else
            {
                cmd.CommandText = sql;
                cmd.Parameters.Clear();
                cmd.Parameters.AddWithValue("@id", id);
                cmd.ExecuteNonQuery();
            }
        }
    }
}