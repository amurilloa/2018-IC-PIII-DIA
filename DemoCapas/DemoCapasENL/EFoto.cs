﻿using System.Drawing;

namespace DemoCapasENL
{
    public class EFoto
    {
        public int Id { get; set; }
        public Image Imagen { get; set; }

        public override string ToString()
        {
            return Id.ToString();
        }
    }
}
