﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PracticaUno
{
    class Program
    {
        static void Main(string[] args)
        {
            string menu = "\nMenu Practica Uno\n" +
                "1. Sumar\n" +
                "2. Restar\n" +
                "3. Multiplicar\n" +
                "4. Dividir\n" +
                "5. Llenar Arreglo\n" +
                "6. Imprimir Arreglo\n" +
                "7. Salir\n" +
                "Seleccione una opción: ";
            bool salir = false;
            Operaciones ope = new Operaciones();
            int[] arreglo = null;

            while (!salir)
            {
                Console.Clear();
                Console.Write(menu);
                int op = Int32.Parse(Console.ReadLine());
                int n1 = 10;
                int n2 = 2;
                switch (op)
                {
                    case 1:
                        int res = ope.Sumar(n1, n2);
                        Console.WriteLine("{0}+{1}={2}", n1, n2, res);
                        break;
                    case 2:
                        res = ope.Restar(n1, n2);
                        Console.WriteLine("{0}-{1}={2}", n1, n2, res);
                        break;
                    case 3:
                        res = ope.Multiplicar(n1, n2);
                        Console.WriteLine("{0}*{1}={2}", n1, n2, res);
                        break;
                    case 4:
                        res = ope.Dividir(n1, n2);
                        Console.WriteLine("{0}/{1}={2}", n1, n2, res);
                        break;
                    case 5:
                        Console.Write("Digite el tamaño: ");
                        int dim = Int32.Parse(Console.ReadLine());
                        //arreglo = ope.LlenarArreglo(dim);
                        ope.LlenarArreglo(ref arreglo, dim);
                        break;
                    case 6:
                        Console.WriteLine(ope.ImprimirArreglo(arreglo));
                        break;
                    case 7:
                        Console.WriteLine("Salir");
                        salir = true;
                        break;
                    default:
                        Console.WriteLine("Opción Inválida");
                        break;
                }

                Console.ReadKey();
            }
        }

    }
}