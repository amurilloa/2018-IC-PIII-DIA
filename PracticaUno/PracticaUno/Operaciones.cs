﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PracticaUno
{
    class Operaciones
    {
        internal int Sumar(int n1, int n2)
        {
            return n1 + n2;
        }

        internal int Restar(int n1, int n2)
        {
            return n1 - n2;
        }

        internal int Multiplicar(int n1, int n2)
        {
            return n1 * n2;
        }

        internal int Dividir(int n1, int n2)
        {
            if (n2 == 0)
            {
                return 0;
            }
            return n1 / n2;
        }

        /// <summary>
        /// Crea un arreglo y lo llena con el tamaño del mismo
        /// </summary>
        /// <param name="dim">Tamaño del arreglo</param>
        /// <returns>Arreglo</returns>
        internal int[] LlenarArreglo(int dim)
        {
            int[] arreglo = new int[dim];
            for (int i = 0; i < arreglo.Length; i++)
            {
                arreglo[i] = dim;
            }
            return arreglo;
        }

        /// <summary>
        /// Obtiene un arreglo por referencia, lo crea y lo llena con
        /// el valor del tamaño
        /// </summary>
        /// <param name="arreglo">Referencia del arreglo</param>
        /// <param name="dim">Tamaño del arreglo</param>
        internal void LlenarArreglo(ref int[] arreglo, int dim)
        {
            arreglo = new int[dim];
            for (int i = 0; i < arreglo.Length; i++)
            {
                arreglo[i] = dim;
            }

        }

        internal string ImprimirArreglo(int[] arreglo)
        {
            string texto = "";
            if (arreglo != null)
            {
                foreach (var item in arreglo)
                {
                    texto += item + ",";
                }
            }
            return texto.Length > 0 ? texto.Substring(0, texto.Length - 1) : texto;
        }
    }
}
