﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PracticaUno.Tres
{
    class Program
    {
        static void Main(string[] args)
        {
            Logica log = new Logica();
            int n1 = 48;//PracticaUno.Dos.Program.LeerInt("Digite #1");
            int n2 = 60;// PracticaUno.Dos.Program.LeerInt("Digite #2");
            Console.WriteLine("mcd({0},{1}) = {2}", n1, n2, log.MCD(n1, n2));
            int meses = 8;//PracticaUno.Dos.Program.LeerInt("Digite la cantidad de meses");
            Console.WriteLine("En {0} meses tendrá {1} conejos", meses, log.CanConejos(meses));
            int conejos = 110;// PracticaUno.Dos.Program.LeerInt("Digite la cantidad de meses");
            Console.WriteLine("Tendrá los {0} conejos en {1} meses", conejos, log.CanMeses(conejos));
            Console.WriteLine("Tendrá los {0} conejos en {1} meses", conejos, log.CanMesesDos(conejos));

            Console.WriteLine("8 es: {0}",log.EsPrimo(8));
            Console.WriteLine("7 es: {0}",log.EsPrimo(7));
            Console.WriteLine("Tiempo en obtener 10k de números primos: {0:0.0}",log.DiezMilPrimos());

            Console.WriteLine("8 es perfecto: {0}", log.EsPerfecto(8));
            Console.WriteLine("7 es perfecto: {0}", log.EsPerfecto(7));
            Console.WriteLine("6 es perfecto: {0}", log.EsPerfecto(6));
            Console.WriteLine("28 es perfecto: {0}", log.EsPerfecto(28));

            Console.ReadKey();
        }
    }
}
