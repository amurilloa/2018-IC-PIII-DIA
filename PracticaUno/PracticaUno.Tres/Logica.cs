﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PracticaUno.Tres
{
    class Logica
    {
        /// <summary>
        /// Obtiene el máximo común divisor de dos números
        /// </summary>
        /// <param name="n1">Número 1</param>
        /// <param name="n2">Número 2</param>
        /// <returns>Máximo Común Divisor</returns>
        internal int MCD(int n1, int n2)
        {
            int mcd = 1;
            int div = 2;
            while (n1 >= div && n2 >= div)
            {
                if (n1 % div == 0 && n2 % div == 0)
                {
                    mcd *= div;
                    n1 /= div;
                    n2 /= div;
                    div = 2;
                }
                else
                {
                    div++;
                }
            }
            return mcd;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="meses"></param>
        /// <returns></returns>
        internal int CanConejos(int meses)
        {
            int fertiles = 1;
            int crias = 0;
            for (int i = 0; i < meses; i++)
            {
                int temp = fertiles;
                fertiles = fertiles + crias;
                crias = temp;
            }
            return (crias + fertiles) * 2;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="conejos"></param>
        /// <returns></returns>
        internal int CanMeses(int conejos)
        {
            int meses = 0;
            while (conejos > CanConejos(meses))
            {
                meses++;
            }
            return meses;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="conejos"></param>
        /// <returns></returns>
        internal int CanMesesDos(int conejos)
        {
            int meses = 0;
            int canConejos = 0;
            int fertiles = 1;
            int crias = 0;
            while (conejos > canConejos)
            {
                int temp = fertiles;
                fertiles = fertiles + crias;
                crias = temp;
                meses++;
                canConejos = (fertiles + crias) * 2;
            }
            return meses;
        }

        internal bool EsPrimo(int numero)
        {
            int div = 0;
            for (int i = 1; i <= numero; i++)
            {
                if (numero % i == 0)
                {
                    div++;
                }
                if (div > 2)
                {
                    return false;
                }
            }
            return div == 2;
        }

        internal double DiezMilPrimos()
        {
            DateTime di = DateTime.Now;
            int con = 0;
            int num = 0;
            while (con < 10000)
            {
                if (EsPrimo(num))
                {
                    con++;
                }
                num++;
            }
            DateTime df = DateTime.Now;
            Console.WriteLine(num);
            return df.Subtract(di).TotalSeconds;
        }

        internal bool EsPerfecto(int numero)
        {
            int sumDiv = 0;
            for (int i = 1; i < numero; i++)
            {
                if (numero % i == 0)
                {
                    sumDiv += i;
                }
            }
            return sumDiv == numero;
        }

    }
}
