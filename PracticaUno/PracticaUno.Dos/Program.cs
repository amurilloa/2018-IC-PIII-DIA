﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PracticaUno.Dos
{
    public class Program
    {
        public static int LeerInt(string mensaje)
        {
            int n1;
            do
            {
                Console.Write(mensaje + ": ");
            } while (!Int32.TryParse(Console.ReadLine(), out n1));

            return n1;
        }
        static void Main(string[] args)
        {
            Logica log = new Logica();
            //Console.Write("Digite su nombre: ");
            //string nombre = Console.ReadLine();
            //Console.WriteLine("Hola {0}, hoy es {1}",nombre, DateTime.Now);

            //while (true)
            //{
            //    Console.Clear();
            //    int n1 = LeerInt("Digite el número 1");
            //    int n2 = LeerInt("Digite el número 2");
            //    int n3 = LeerInt("Digite el número 3");

            //    Console.WriteLine("El resultado es: {0}", log.Operacion(n1, n2, n3));
            //    Console.Write("s: Salir, otra para repetir: ");
            //    char con = Console.ReadKey().KeyChar;
            //    if (con == 's')
            //    {
            //        break;
            //    }
            //}
            //int num = LeerInt("Digite un número positivo");
            //Console.WriteLine("{0}-->{1}",num, log.Invertir(num));

            int cant = LeerInt("Digite la cantidad de numeros");
            int[] arreglo = new int[cant];
            for (int i = 0; i < arreglo.Length; i++)
            {
                arreglo[i] = LeerInt("Digite el #" + (i + 1));
            }
            Console.WriteLine("El menor es {0}", log.Menor(arreglo));
            Console.WriteLine("El mayor es {0}", log.Mayor(arreglo));
            Console.WriteLine("Promedio es {0}", log.Promedio(arreglo));
            Console.ReadKey();
        }
    }
}
