﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PracticaUno.Dos
{
    class Logica
    {
        /// <summary>
        /// Obtiene el producto de dos números si el primero es positivo o
        /// en caso contrario obtiene la suma de dos números
        /// </summary>
        /// <param name="n1">Número 1</param>
        /// <param name="n2">Número 2</param>
        /// <param name="n3">Número 3</param>
        /// <returns>Suma o Multiplicación de números</returns>
        internal int Operacion(int n1, int n2, int n3)
        {
            return n1 > 0 ? n2 * n3 : n2 + n3;
            //if (n1 > 0)
            //{
            //    return n2 * n3;
            //}
            //return n2 + n3;
        }

        internal int Invertir(int num)
        {
            int inv = 0;
            while (num != 0)
            {
                int ult = num % 10;
                inv = inv * 10 + ult;
                num /= 10;
            }
            return inv;
        }

        internal int Menor(int[] arreglo)
        {
            int menor = arreglo[0];
            foreach (var item in arreglo)
            {
                if (item < menor)
                {
                    menor = item;
                }
            }
            return menor;
        }

        internal int Mayor(int[] arreglo)
        {
            int mayor = arreglo[0];
            foreach (var item in arreglo)
            {
                if (item > mayor)
                {
                    mayor = item;
                }
            }
            return mayor;
        }

        internal int Promedio(int[] arreglo)
        {
            int sum = 0;
            foreach (var item in arreglo)
            {
                sum += item;
            }
            return sum / arreglo.Length;
        }
    }
}
