﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PracticaDos
{
    class Mesa
    {
        public int Numero { get; set; }
        public Producto[] Menu { get; set; }
        public int[] Pedido { get; set; }

        public Mesa()
        {
            Menu = new Producto[]
            {
                new Producto{Descripcion="Hamburguesa Sencilla", Precio=15},
                new Producto{Descripcion="Hamburguesa Queso", Precio=18},
                new Producto{Descripcion="Hamburguesa Especial", Precio=20},
                new Producto{Descripcion="Papas Fritas", Precio=8},
                new Producto{Descripcion="Refresco", Precio=5},
                new Producto{Descripcion="Postre", Precio=6}
            };
            Pedido = new int[Menu.Length];
        }

        internal void CapturarOrden(int producto, int cantidad)
        {
            Pedido[producto - 1] += cantidad;
        }

        internal string MostrarMenu()
        {
            string menu = "";
            for (int i = 0; i < Menu.Length; i++)
            {
                menu += String.Format("{0}. {1}{2}(${3})\n", (i + 1),
                    Menu[i].Descripcion,
                    Menu[i].Descripcion.Length > 12 ? "\t" : "\t\t",
                    Menu[i].Precio);
            }
            return menu;
        }

        internal double CalcularCuenta()
        {
            double total = 0;
            for (int i = 0; i < Pedido.Length; i++)
            {
                total += Pedido[i] * Menu[i].Precio;
            }
            return total;
        }
    }
}
