﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PracticaDos
{
    class Telefono
    {
        private int duracion;

        public int Duracion
        {
            get { return duracion; }
            set { duracion = value >= 0 ? value : 0; }
        }

        internal double Costo()
        {
            double total = 5 ;

            Duracion = Duracion >= 3 ? Duracion - 3 : 0;

            total += Duracion * 3;

            return total;
        }
    }
}
