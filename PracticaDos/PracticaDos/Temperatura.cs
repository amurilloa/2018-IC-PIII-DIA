﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PracticaDos
{
    class Temperatura
    {
        public double Celsius { get; set; }

        /// <summary>
        /// Convierte los grados celsius a farenheit
        /// </summary>
        /// <returns>Grados Farenheit</returns>
        internal double CelsiusToFarenheit()
        {
            return Celsius * 9 / 5 + 32;
        }
    }
}
