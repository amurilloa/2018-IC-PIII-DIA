﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PracticaDos
{
    class Agencia
    {
        public const double GANANCIA = 0.12;
        public const double IMPUESTO = 0.06;

        internal double CostoFinal(double costoProduccion)
        {
            return (costoProduccion * GANANCIA) +
                (costoProduccion * IMPUESTO) +
                costoProduccion;
        }
    }
}
