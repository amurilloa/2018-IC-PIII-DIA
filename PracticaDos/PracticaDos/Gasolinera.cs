﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PracticaDos
{
    class Gasolinera
    {
        public const double LITROS_X_GAL = 3.78541;

        public double Precio { get; set; }

        public Gasolinera()
        {
            Precio = 672;
        }

        internal double CalcularPago(double galones)
        {
            return galones * LITROS_X_GAL * Precio;
        }
    }
}
