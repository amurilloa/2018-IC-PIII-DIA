﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PracticaDos
{
    class Program
    {
        public static int LeerInt(string mensaje)
        {
            int n1;
            do
            {
                Console.Write(mensaje + ": ");
            } while (!Int32.TryParse(Console.ReadLine(), out n1));

            return n1;
        }

        static void Main(string[] args)
        {
            #region Pruebas
            //Circunferecia rueda = new Circunferecia { Radio = 10.2 };
            //Circunferecia moneda = new Circunferecia { Radio = 1.4 };
            //Console.WriteLine("Perimetro de la rueda {0:0.00}", rueda.CalcularPerimetro());

            //Rectangulo pared = new Rectangulo { Largo = 6, Ancho = 3 };
            //Rectangulo ventana = new Rectangulo { Largo = 2, Ancho = 1 };
            //double area = pared.CalcularArea() - ventana.CalcularArea();
            //Console.WriteLine("Área a Pintar: {0}m²", area);
            //double min = area * 10;
            //int horas = (int)min / 60;
            //int m = (int)min % 60;
            //Console.WriteLine("Se tardará pintando la pared {0}hrs{1}min", horas, m);

            //Fecha fec = new Fecha(29, 10, 2016);

            //Console.WriteLine(fec.MostrarFecha());
            //fec.ModificarFecha(1, 2, 2000);
            //Console.WriteLine(fec.MostrarFecha());
            //Console.WriteLine(fec.MostrarFechaL());

            //Articulo art = new Articulo(1, "Arroz", 2030, 58)
            //{
            //    Clave = 123,
            //    Descripcion = "Arroz",
            //    Precio = 2030,
            //    Cantidad = 10
            //};

            //Console.WriteLine("El {0} paga {1:0.00} CRC de IVA", art.Descripcion, art.CalcularIVA());

            //Temperatura temp = new Temperatura { Celsius = 30 };
            //Console.WriteLine("{0:0}C son {1:0}F", temp.Celsius, temp.CelsiusToFarenheit());

            //CambioDivisas cd = new CambioDivisas(568.18);
            //cd.Colones = 100000;
            //Console.WriteLine("{0:0.00} CRC son {1:0.00} USD", cd.Colones, cd.ConvertirADolares());

            //Gasolinera gas = new Gasolinera();
            //Console.WriteLine("El costo por 5 gal es {0:F2} CRC", gas.CalcularPago(5));
            #endregion

            #region Mesas

            //string opciones = "\nMenu Principal\n\n" +
            //    "1. Capturar Orden\n" +
            //    "2. Calcular Total\n" +
            //    "3. Salir\n" +
            //    "Seleccione una opción";
            //Mesa mesa = new Mesa();

            //while (true)
            //{
            //    Console.Clear();
            //    int op = LeerInt(opciones);
            //    if (op == 3)
            //    {
            //        break;
            //    }

            //    if (op == 1)
            //    {
            //        while (true)
            //        {
            //            Console.Clear();
            //            op = LeerInt(mesa.MostrarMenu() + "7. Atras\nSeleccione una opción");
            //            if (op == 7)
            //            {
            //                break;
            //            }
            //            int can = LeerInt("Cantidad solicitada");
            //            mesa.CapturarOrden(op, can);
            //        }
            //    }

            //    if (op == 2)
            //    {
            //        Console.Clear();
            //        Console.WriteLine("El monto a cancelar es: ${0}", mesa.CalcularCuenta());
            //        mesa = new Mesa();
            //        Console.ReadKey();
            //    }

            //}
            #endregion

            //Agencia agencia = new Agencia();
            //Console.WriteLine(agencia.CostoFinal(3000000));

            //Logica log = new Logica();
            //Console.WriteLine(log.DiasASegundos(3));

            Telefono tel = new Telefono { Duracion = 5};
            Console.WriteLine("Costo de la llamada ${0}", tel.Costo());

            Console.ReadKey();
        }
    }
}
