﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PracticaDos
{
    class Fecha
    {
        public int Dia { get; private set; }
        public int Mes { get; private set; }
        public int Anno { get; private set; }

        public Fecha()
        {
            Dia = 1;
            Mes = 1;
            Anno = 1900;
        }

        public Fecha(int dia, int mes, int anno)
        {
            ModificarFecha(dia, mes, anno);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dia"></param>
        /// <param name="mes"></param>
        /// <param name="anno"></param>
        internal void ModificarFecha(int dia, int mes, int anno)
        {
            if (ValidarFecha(dia, mes, anno))
            {
                Dia = dia;
                Mes = mes;
                Anno = anno;
            }
            else
            {
                Dia = 1;
                Mes = 1;
                Anno = 1900;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dia"></param>
        /// <param name="mes"></param>
        /// <param name="anno"></param>
        /// <returns></returns>
        private bool ValidarFecha(int dia, int mes, int anno)
        {
            if (dia < 1 || dia > 31)
            {
                return false;
            }
            if (mes < 1 || mes > 12)
            {
                return false;
            }

            if (anno < 0)
            {
                return false;
            }

            if (dia > 30 && (mes == 4 || mes == 6 || mes == 9 || mes == 11))
            {
                return false;
            }

            bool bis = EsBisiesto(anno);

            if (mes == 2 && ((!bis && dia > 28) || (bis && dia > 29)))
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="anno"></param>
        /// <returns></returns>
        internal bool EsBisiesto(int anno)
        {
            if (anno % 4 == 0)
            {
                if (anno % 100 == 0)
                {
                    return anno % 400 == 0;
                }
                return true;
            }
            return false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        internal string MostrarFecha()
        {
            string d = Dia >= 10 ? Dia.ToString() : "0" + Dia;
            string m = Mes >= 10 ? Mes.ToString() : "0" + Mes;
            return String.Format("{0}\\{1}\\{2}", d, m, Anno);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        internal string MostrarFechaL()
        {
            EMes m = (EMes)Mes;
            return String.Format("{0} de {1} de {2}", Dia, m, Anno);
        }
    }
}