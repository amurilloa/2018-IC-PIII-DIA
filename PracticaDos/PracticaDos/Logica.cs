﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PracticaDos
{
    class Logica
    {
        public const int HD = 24;
        public const int MH = 60;
        public const int SM = 60;


        internal double DiasASegundos(int dias)
        {
            return dias * HD * MH * SM;
        }
    }
}
