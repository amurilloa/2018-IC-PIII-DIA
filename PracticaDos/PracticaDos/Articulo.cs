﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PracticaDos
{
    class Articulo
    {
        public int Clave { get; set; }
        public string Descripcion { get; set; }
        public double Precio { get; set; }
        public int Cantidad { get; set; }

        public Articulo(int clave, string descripcion, double precio, int cantidad)
        {
            Clave = clave;
            Descripcion = descripcion;
            Precio = precio;
            Cantidad = cantidad;
        }

        internal double CalcularIVA()
        {
            return Precio * 0.13;
        }
    }
}
